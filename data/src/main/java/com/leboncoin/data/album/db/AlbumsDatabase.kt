package com.leboncoin.data.album.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.leboncoin.data.album.dto.AlbumSourcesEntity

/**
 * Created by Amancio on 20/08/22
 */
@Database(entities = [AlbumSourcesEntity::class], version = 2, exportSchema = false)
abstract class AlbumsDatabase : RoomDatabase() {
    abstract fun getAlbumsDao(): AlbumsDao

    companion object {
        private const val DATABASE = "BonCoinAlbums"
        @Volatile
        private lateinit var db: AlbumsDatabase

        fun getDatabase(context: Context): AlbumsDatabase {
            synchronized(this) {
                if (!::db.isInitialized) {
                    db = Room.databaseBuilder(context, AlbumsDatabase::class.java, DATABASE)
                        .fallbackToDestructiveMigration()
                        .build()
                }
                return db
            }
        }

        fun getInMemoryDb(context: Context): AlbumsDatabase {
            synchronized(this) {
                if (!::db.isInitialized) {
                    db = Room.inMemoryDatabaseBuilder(context, AlbumsDatabase::class.java)
                        .fallbackToDestructiveMigration()
                        .build()
                }
                return db
            }
        }
    }
}
