package com.leboncoin.data.album.repo

import androidx.paging.*
import androidx.room.withTransaction
import com.leboncoin.data.album.apiservice.AlbumApi
import com.leboncoin.data.album.db.AlbumsDao
import com.leboncoin.data.album.db.AlbumsDatabase
import com.leboncoin.data.album.dto.AlbumSourcesEntity
import com.leboncoin.data.album.mapper.AlbumsEntityMapper
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

/**
 * Created by Amancio on 20/08/22
 * remote mediator
 */
@OptIn(ExperimentalPagingApi::class)
class PageRemoteMediator(
    private val db: AlbumsDatabase,
    private val redditApi: AlbumApi,
    private val mapper: AlbumsEntityMapper,
) : RemoteMediator<Int, AlbumSourcesEntity>() {
    private val albumDao: AlbumsDao = db.getAlbumsDao()

    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }
    
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, AlbumSourcesEntity>
    ): MediatorResult {
        Timber.d( "load data from remote ")
        return try {
            // Get the closest item from PagingState that we want to load data around.
            val data = redditApi.getAlbums()
            val items = mapper.asDataModel(data)
            db.withTransaction {
                albumDao.saveAlbums(items)
            }
            MediatorResult.Success(endOfPaginationReached = items.isNotEmpty())
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }
    }
}
