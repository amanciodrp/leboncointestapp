package com.leboncoin.data.album.repo

import androidx.paging.*
import com.leboncoin.data.album.apiservice.AlbumApi
import com.leboncoin.data.album.db.AlbumsDatabase
import com.leboncoin.data.album.mapper.AlbumsEntityMapper
import com.leboncoin.domain.album.AlbumRepository
import com.leboncoin.domain.album.entity.AlbumItem

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber

/**
 * Created by Amancio on 18/08/22
 */
class AlbumsRepositoryImpl(
    private val db: AlbumsDatabase,
    remoteApi: AlbumApi,
    private val mapper: AlbumsEntityMapper
) : AlbumRepository, CoroutineScope by CoroutineScope(
    Dispatchers.IO
) {

    private val remoteMediator = PageRemoteMediator(db, remoteApi, mapper)

    /**
     * get albums
     */
    @OptIn(ExperimentalPagingApi::class)
    override fun getAlbums(): Flow<PagingData<AlbumItem>> = Pager(
        config = getDefaultPageConfig(),
        remoteMediator = remoteMediator,
    ) {
        db.getAlbumsDao().getAllAlbums()
    }.flow
        .map { pagingData ->
            pagingData
                // Map data Album model to common domain model.
                .map { album -> mapper.asAlbumItem(album) }
        }

    /**
     * let's define page size, page size is the only required param, rest is optional
     */
    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = DEFAULT_PAGE_SIZE, enablePlaceholders = false)
    }

    companion object {
        private const val DEFAULT_PAGE_SIZE = 40
    }
}