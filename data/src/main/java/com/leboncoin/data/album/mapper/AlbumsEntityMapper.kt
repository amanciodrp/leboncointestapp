package com.leboncoin.data.album.mapper

import com.leboncoin.data.album.dto.AlbumSourcesEntity
import com.leboncoin.domain.album.entity.AlbumItem

/**
 * Created by Amancio on 20/08/22
 */
class AlbumsEntityMapper  {

    fun asAlbumItem(album: AlbumSourcesEntity) = AlbumItem(
        albumId = album.albumId,
        id = album.id,
        title = album.title,
        url = album.url,
        thumbnailUrl = album.thumbnailUrl!!,
    )

    fun asAlbumSourcesEntity(album: AlbumItem) = AlbumSourcesEntity(
        albumId = album.albumId,
        id = album.id,
        title = album.title,
        url = album.url,
        thumbnailUrl = album.thumbnailUrl,
    )

    /**
     * Convert Network results to database objects
     */
    fun asDataModel(albums: List<AlbumItem>): List<AlbumSourcesEntity> {
        return albums.map {
            AlbumSourcesEntity(
                albumId = it.albumId,
                id = it.albumId,
                title = it.title,
                url = it.url,
                thumbnailUrl = it.thumbnailUrl
            )
        }
    }

}