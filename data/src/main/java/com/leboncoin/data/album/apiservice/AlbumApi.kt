package com.leboncoin.data.album.apiservice

import com.leboncoin.domain.album.entity.AlbumItem
import retrofit2.http.GET

/**
 * Created by Amancio 11/08/22
 */
interface AlbumApi {
    @GET("/img/shared/technical-test.json")
    suspend fun getAlbums(): List<AlbumItem>
}