package com.leboncoin.data.album.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Amancio on 20/08/22
 */
@Entity(tableName = "Albums")
data class AlbumSourcesEntity(
    @PrimaryKey var id: Int = 0,
    @ColumnInfo(name = "albumId") var albumId: Int = 0,
    @ColumnInfo(name = "title") var title: String = "",
    @ColumnInfo(name = "url") var url: String = "",
    @ColumnInfo(name = "thumbnailUrl") var thumbnailUrl: String? = "",
)