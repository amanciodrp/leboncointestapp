package com.leboncoin.data.album.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leboncoin.data.album.dto.AlbumSourcesEntity
import com.leboncoin.domain.album.entity.AlbumItem

/**
 * Created by Amancio on 20/08/22
 */
@Dao
interface AlbumsDao {

    @Query("SELECT * FROM Albums ORDER BY albumId COLLATE NOCASE ASC")
    fun getAllAlbums(): PagingSource<Int, AlbumSourcesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAlbums(albums: List<AlbumSourcesEntity>)

    @Query("SELECT * FROM Albums WHERE id = :id")
    fun getAlbum(id: Int): AlbumSourcesEntity?

    @Query("SELECT * FROM Albums LIMIT 1")
    fun getAnyAlbum(): AlbumSourcesEntity?

    @Query("DELETE FROM Albums")
    fun clear()

}