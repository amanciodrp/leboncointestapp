package com.leboncoin.domain.album.usecase

import com.leboncoin.domain.album.AlbumRepository

class AlbumsUseCase(
    private val repositories: AlbumRepository
) {
    fun getAlbums() = repositories.getAlbums()
}