package com.leboncoin.domain.album

import androidx.paging.PagingData
import com.leboncoin.domain.album.entity.AlbumItem
import kotlinx.coroutines.flow.Flow

/**
 * Created by Amancio on 16/08/2022
 */
interface AlbumRepository {

    /**
     * get albums from cache
     */
    fun getAlbums(): Flow<PagingData<AlbumItem>>
}