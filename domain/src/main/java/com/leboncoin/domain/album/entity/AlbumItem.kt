package com.leboncoin.domain.album.entity

/**
 * Created by Amancio on 18/08/22
 * remote album item data class
 */
data class AlbumItem(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)