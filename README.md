# LeBonCoinTestApp
LeBonCoinTest App is a application to download albums from leboncoin static backend for test. 

The application is based on MVVM pattern and clean architecture using common Android Application tech-stach. Application uses the library Koin for dependency injection.

##  Tech stack & libraries
 - Minimum SDK level 24
 - Kotlin based, Coroutines
 - Koin for dependency injection.

## JetPack
 - Flow - notify domain layer data to views.
 - Lifecycle - dispose of observing data when lifecycle state changes.
 - ViewModel - UI related data holder, lifecycle aware.
 - Paging 3 - is usefull to load and display pages of data from a larger dataset with local db and remote. This test app load large Json array from leboncoin static backend. a better implementation would be to have a backend with the pagination option in order to progressively load the data. 
The use of paging 3 in this test gives an impetus for a future implementation of paging
 - Room Persistence - construct a database using the abstract layer.

### Picasso - 
    A powerful image downloading and caching library for Android

### Retrofit2 & OkHttp3 -
    Construct the REST APIs and paging network data.

### Gson -
    JSON library

### Timber -
    logging library
    
### Material-Components -
    Material design components like cardView.

### Architecture
    MVVM Architecture (View - DataBinding - ViewModel - Model)

![plot](/doc/archi.png)

### Clean Architecture
![plot](/doc/appLayer.png)

### screenshot
![Screenshot](./doc/screen.png)

