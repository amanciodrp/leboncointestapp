package com.leboncoin.testapp.utils

import android.content.Context
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import okhttp3.Route
import com.squareup.picasso.OkHttp3Downloader
import okhttp3.Response

/**
 * Created by Amancio on 20/08/22
 */
object MyPicasso {

    fun getImageLoader(context: Context?): Picasso? {
        var sPicasso: Picasso? = null
        val okHttpClient = OkHttpClient.Builder()
            .authenticator { _: Route?, response: Response ->
                response.request().newBuilder()
                    .header("User-Agent", "credential")
                    .build()
            }.build()
        
        if (sPicasso == null) {
            val builder = Picasso.Builder(context!!)
            builder.downloader(OkHttp3Downloader(okHttpClient))
            sPicasso = builder.build()
        }
        return sPicasso
    }
}