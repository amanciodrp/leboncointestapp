package com.leboncoin.testapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.leboncoin.testapp.adapter.AlbumsAdapter.AlbumViewHolder.Companion.from
import com.leboncoin.testapp.databinding.AlbumItemBinding
import com.leboncoin.testapp.model.UserAlbumItem
import com.leboncoin.testapp.model.AlbumsDiffUtilCallback

/**
 * Created by Amancio on 16/08/22
 * [RecyclerView.Adapter] that can display a.
 */
class AlbumsAdapter :
    PagingDataAdapter<UserAlbumItem, AlbumsAdapter.AlbumViewHolder>(AlbumsDiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        return from(parent)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        val album = getItem(position)
        holder.bind(album)
    }

    /**
     * Album view holder
     */
    class AlbumViewHolder private constructor(private val binding: AlbumItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): AlbumViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = AlbumItemBinding.inflate(inflater, parent, false)
                return AlbumViewHolder(binding)
            }
        }

        /**
         * bind with viewmodel and album
         */
        fun bind(album: UserAlbumItem?) {
            binding.album = album
            binding.executePendingBindings()
        }
    }

}