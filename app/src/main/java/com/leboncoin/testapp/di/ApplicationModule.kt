package com.leboncoin.testapp.di

import androidx.lifecycle.SavedStateHandle
import com.leboncoin.data.album.apiservice.AlbumApi
import com.leboncoin.data.album.apiservice.createNetworkClient
import com.leboncoin.data.album.db.AlbumsDatabase
import com.leboncoin.data.album.mapper.AlbumsEntityMapper
import com.leboncoin.data.album.repo.AlbumsRepositoryImpl
import com.leboncoin.domain.album.AlbumRepository
import com.leboncoin.domain.album.usecase.AlbumsUseCase
import com.leboncoin.testapp.BuildConfig
import com.leboncoin.testapp.mapper.UserUiMapper
import com.leboncoin.testapp.ui.main.AlbumViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * ViewModel module
 */
val viewModel: Module = module {
    viewModel {
        AlbumViewModel(
            albumsUseCase = get(),
            mapper = UserUiMapper(),
            savedStateHandle = SavedStateHandle()
        )
    }
}

/**
 * Use Case module
 */
val useCaseModules: Module = module {
    factory { AlbumsUseCase(repositories = get()) }
}

/**
 * Repository module
 */
val repositoryModules: Module = module {
    single<AlbumRepository> { AlbumsRepositoryImpl(get(), get(), AlbumsEntityMapper()) }
}

/**
 * Network module
 */
val networkModule: Module = module {
    single { createNetworkClient(BuildConfig.BACKEND_URL) }
    single { (get() as Retrofit).create(AlbumApi::class.java) }
}

/**
 * Room db module
 */
val databaseModule: Module = module {
    single {
        AlbumsDatabase.getDatabase(androidContext())
    }
}

/**
 * Room in memory db module
 */
val dbInMemoryModule: Module = module {
    single {
        AlbumsDatabase.getInMemoryDb(androidContext())
    }
}
