package com.leboncoin.testapp

import android.app.Application
import android.content.Context
import com.leboncoin.testapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext
import org.koin.core.module.Module
import timber.log.Timber

/**
 * Created by Amancio on 18/08/22
 */
class App: Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
        initTimber() // init logger
        loadKoin() // load koin module
        printVersion() // print app version
    }

    /**
     * load koin module
     */
    private fun loadKoin() {
        GlobalContext.startKoin {
            // use the Android context given there
            androidContext(appContext)
            androidLogger()
            // load properties from assets/koin.properties file
            androidFileProperties()
            // load module
            modules(appModules())
        }
    }

    /**
     * create module list
     */
    private fun appModules(): List<Module> {
        return listOf(
            networkModule,
            useCaseModules,
            repositoryModules,
            databaseModule,
            viewModel,
        )
    }

    /**
     * print app infos (package and version name)
     */
    private fun printVersion() {
        Timber.i("#########################################################")
        Timber.i("----------- beginning to create of ${BuildConfig.APPLICATION_ID} -----------")
        Timber.i("----------- Version name : ${BuildConfig.VERSION_NAME} -----------")
        Timber.i("#########################################################")
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG || isLoggerNeeded) {
            Timber.plant(Timber.DebugTree())
            Timber.d("Timber configured")
        }
    }

    companion object {
        lateinit var appContext: Context
        private const val isLoggerNeeded = true
    }
}