package com.leboncoin.testapp.binding

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.leboncoin.testapp.R
import com.leboncoin.testapp.utils.MyPicasso
import com.squareup.picasso.Callback
import timber.log.Timber

@BindingAdapter("bind:loadImage")
fun loadImage(view: AppCompatImageView, url: String) {
    if (url.isEmpty()) return
    url.let {
        MyPicasso.getImageLoader(view.context)?.load(url)
            ?.fit()
            ?.noFade()
            ?.centerCrop()
            ?.placeholder(R.drawable.leboncoin_icon)
            ?.into(view, object : Callback {
                override fun onSuccess() {
                    view.alpha = 0f
                    view.animate().setDuration(200).alpha(1f).start()
                }

                override fun onError(e: java.lang.Exception?) {
                    Timber.d(" onBindViewHolder exception $e")
                }
            })
    }
}