package com.leboncoin.testapp.ui.main

import androidx.lifecycle.*
import androidx.paging.*

import com.leboncoin.domain.album.usecase.AlbumsUseCase
import com.leboncoin.testapp.mapper.UserUiMapper
import com.leboncoin.testapp.model.UserAlbumItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import timber.log.Timber

/**
 * Created by Amancio on 19/08/22
 */
class AlbumViewModel(
    private val albumsUseCase: AlbumsUseCase,
    private val mapper: UserUiMapper,
    private val savedStateHandle: SavedStateHandle
) : ViewModel(), LifecycleObserver {

    companion object {
        const val KEY_ALBUM = "albums"
        const val DEFAULT_DATA = "leboncoinAlbum"
    }

    lateinit var albums: Flow<PagingData<UserAlbumItem>>

    init {
        if (!savedStateHandle.contains(KEY_ALBUM)) {
            savedStateHandle[KEY_ALBUM] = DEFAULT_DATA
        }
        fetchAlbum() // fetch Album
    }

    // fetch album from db with remote merge
    @OptIn(ExperimentalCoroutinesApi::class)
    fun fetchAlbum(){
        Timber.d("fetchAlbum ")
        albums = savedStateHandle.getLiveData<String>(KEY_ALBUM)
            .asFlow()
            .flatMapLatest {
                albumsUseCase.getAlbums().map { pagingData -> pagingData
                    .map { mapper.toEntity(it) }
                }
            }.cachedIn(viewModelScope)
    }
}
