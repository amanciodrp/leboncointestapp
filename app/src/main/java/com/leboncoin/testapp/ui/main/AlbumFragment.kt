package com.leboncoin.testapp.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.leboncoin.testapp.adapter.AlbumsAdapter
import com.leboncoin.testapp.databinding.FragmentMainBinding
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.paging.LoadState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope

/**
 * Created by Amancio 17/08/22
 * Album fragment to show list
 */
class AlbumFragment : Fragment(), CoroutineScope by MainScope() {

    companion object {
        fun newInstance() = AlbumFragment()
    }

    private val albumViewModel: AlbumViewModel by viewModel()
    private lateinit var viewDataBinding: FragmentMainBinding
    private lateinit var adapter: AlbumsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewDataBinding = FragmentMainBinding.inflate(inflater, container, false).apply {
            viewModel = albumViewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLifecycleOwner()
        initAdapter()
        publishList()
        initState()
    }

    /**
     * set the view lifecycle owner
     * use data binding for album item
     */
    private fun setLifecycleOwner() {
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
        lifecycle.addObserver(albumViewModel)
    }

    /**
     * init ui state
     */
    private fun initState() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collect { loadStates ->
                viewDataBinding.rvAlbumList.visibility =
                    if (loadStates.mediator?.refresh is LoadState.Loading) View.GONE else View.VISIBLE
                viewDataBinding.swiperefresh.isRefreshing =
                    loadStates.mediator?.refresh is LoadState.Loading
            }
        }
    }

    /**
     * init adapter
     */
    private fun initAdapter() {
        adapter = AlbumsAdapter()
        viewDataBinding.rvAlbumList.adapter = adapter
    }

    /**
     * publish list to adapter
     */
    private fun publishList() {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            albumViewModel.albums.collectLatest {
                adapter.submitData(it)
            }
        }
    }
}