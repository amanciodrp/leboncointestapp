package com.leboncoin.testapp.model

import androidx.recyclerview.widget.DiffUtil

class AlbumsDiffUtilCallback : DiffUtil.ItemCallback<UserAlbumItem>() {

    override fun areItemsTheSame(
        oldItem: UserAlbumItem,
        newItem: UserAlbumItem
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: UserAlbumItem,
        newItem: UserAlbumItem
    ): Boolean {
        return oldItem.id == newItem.id && oldItem.title == newItem.title
    }

}