package com.leboncoin.testapp.model

import java.io.Serializable

/**
 * Created by Amancio 16/08/2022
 */
data class UserAlbumItem(
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
) : Serializable

