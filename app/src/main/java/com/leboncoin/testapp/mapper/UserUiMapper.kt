package com.leboncoin.testapp.mapper

import com.leboncoin.domain.album.entity.AlbumItem
import com.leboncoin.testapp.model.UserAlbumItem

/**
 * Created by Amancio 20/08/22
 */
class UserUiMapper : EntityMapper<UserAlbumItem, AlbumItem> {

    override fun toEntity(domain: AlbumItem) = UserAlbumItem(
        id = domain.id,
        title = domain.title,
        url = domain.url,
        thumbnailUrl = domain.thumbnailUrl,
    )
}
