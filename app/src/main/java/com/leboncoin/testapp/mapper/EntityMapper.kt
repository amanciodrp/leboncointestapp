package com.leboncoin.testapp.mapper

interface EntityMapper<Entity, Domain> {

    /**
     * convert domain data class to ui model
     */
    fun toEntity(domain: Domain): Entity
}
