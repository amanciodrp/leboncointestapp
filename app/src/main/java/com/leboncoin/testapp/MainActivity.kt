package com.leboncoin.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.leboncoin.testapp.ui.main.AlbumFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, AlbumFragment.newInstance())
                .commitNow()
        }
    }
}