package com.leboncoin.testapp

import com.leboncoin.domain.album.entity.AlbumItem
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Amancio on 20/08/22
 */
internal class AlbumFactoryTest {
    private val counter = AtomicInteger(0)

    fun createAlbum(): AlbumItem {
        val id = counter.incrementAndGet()
        return AlbumItem(
            albumId = id+100,
            id = id,
            title = "title $id",
            url = "http://static.leboncoin.app",
            thumbnailUrl = "http://static.leboncoin.app",
        )
    }
}