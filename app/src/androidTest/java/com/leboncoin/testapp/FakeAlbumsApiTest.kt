package com.leboncoin.testapp

import com.leboncoin.data.album.apiservice.AlbumApi
import com.leboncoin.domain.album.entity.AlbumItem
import java.io.IOException

/**
 * Created by Amancio 20/08/22
 * implements the AlbumApi with controllable requests
 */
class FakeAlbumsApiTest : AlbumApi {

    private val albums = mutableSetOf<AlbumItem>()
    var failureMsg: String? = null

    /**
     * add album in fake list
     */
    fun addAlbum(albumItem: AlbumItem) {
        albums.add(albumItem)
    }

    /**
     * get list like api success result
     */
    override suspend fun getAlbums(): List<AlbumItem> {
        failureMsg?.let {
            throw IOException(it)
        }
        return albums.toList()
    }
}