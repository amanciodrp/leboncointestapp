package com.leboncoin.testapp

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.leboncoin.testapp.adapter.AlbumsAdapter
import com.leboncoin.testapp.di.databaseModule
import com.leboncoin.testapp.di.repositoryModules
import com.leboncoin.testapp.di.useCaseModules
import com.leboncoin.testapp.di.viewModel
import com.leboncoin.testapp.ui.main.AlbumViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTestRule

import org.koin.test.KoinTest
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito

/**
 * Create by Amancio on 20/08/22
 * Simple sanity test to ensure data is displayed
 */
class AlbumFragmentTest : KoinTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testAlbumListExist() {
        onView(withId(R.id.rvAlbumList)).check { view, noViewFoundException ->
            if (noViewFoundException != null) {
                throw noViewFoundException
            }

            val recyclerView = view as RecyclerView
            assertEquals(true, recyclerView.adapter?.itemCount!! > 0)
        }
    }
}