package com.leboncoin.testapp

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.leboncoin.data.album.db.AlbumsDatabase
import com.leboncoin.data.album.mapper.AlbumsEntityMapper
import com.leboncoin.testapp.di.dbInMemoryModule
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.test.KoinTest
import org.koin.test.inject

/**
 * Created by Amancio 20/08/22
 * Album DAO test case
 */
@RunWith(AndroidJUnit4::class)
class AlbumDAOTest : KoinTest {

    private val albumDatabase: AlbumsDatabase by inject()
    private val albumFactoryTest = AlbumFactoryTest()
    private val fakeAlbumsApiTest = FakeAlbumsApiTest()
    private val mapper = AlbumsEntityMapper()

    /**
     * Override default Koin configuration to use Room in-memory database
     */
    @Before()
    fun before() {
        // Inject needed component from Koin
        loadKoinModules(dbInMemoryModule)
        // represent fake api
        createAlbumList()
    }

    // create album list
    private fun createAlbumList() {
        for (i in 1..5000) {
            fakeAlbumsApiTest.addAlbum(albumFactoryTest.createAlbum())
        }
    }

    /**
     * Close resources
     */
    @After
    fun tearDown() {
        albumDatabase.close()
    }

    /**
     * save album on in memory db and fail on check cause mapper transform is missing
     */
    @Test
    fun shouldSaveInDb_andFailCompare() = runBlocking{
        // get album dao
        val albumDAO = albumDatabase.getAlbumsDao()
        // get albums
        val albums = fakeAlbumsApiTest.getAlbums()
        // clear in memory db
        albumDAO.clear()
        // transform domain entity to db entity
        val dbAlbum = albums.map { mapper.asAlbumSourcesEntity(it) }
        // save album
        albumDAO.saveAlbums(dbAlbum)
        // Keep id for each one
        val ids = albums.map { it.id }
        // Request one entity per id
        val requestedEntities = ids.map { albumDAO.getAlbum(it) }
        // compare result
        Assert.assertEquals(albums.first(), requestedEntities.first())
    }

    /**
     * save album on in memory db to avoid reel database corrupt
     */
    @Test
    fun shouldSaveInDb_andSuccessCompare() = runBlocking {
        // get album dao
        val albumDAO = albumDatabase.getAlbumsDao()
        // get albums
        val albums = fakeAlbumsApiTest.getAlbums()
        // clear in memory db
        albumDAO.clear()
        // transform domain entity to db entity
        val dbAlbum = albums.map { mapper.asAlbumSourcesEntity(it) }
        // save album
        albumDAO.saveAlbums(dbAlbum)
        // Keep id for each one
        val ids = albums.map { it.id }
        // Request one entity per id
        val requestedEntities = ids.map { albumDAO.getAlbum(it) }
        // get album from db as domain entity
        val savedAlbum = requestedEntities.map { mapper.asAlbumItem(it!!) }
        // compare result
        Assert.assertEquals(albums.last(), savedAlbum.last())
    }
}